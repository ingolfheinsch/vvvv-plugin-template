﻿using System;
using VVVV.PluginInterfaces.V2;

namespace VVVV.Nodes
{
    [PluginInfo(Name = "Plugin Template", Category = "Template", Help = "", Tags = "", Author = "")]
    public class Class1 : IPluginEvaluate
    {
        #region pins
        [Input("Input String", DefaultString = "empty")]
        ISpread<string> FStringInput;

        [Input("Output String")]
        ISpread<string> FStringOutut;

        #endregion pins

        public void Evaluate(int SpreadMax)
        {
        }
    }
}
